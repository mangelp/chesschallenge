package org.mangelp.trycatch.chesschallenge;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class BtCandidateGeneratorTest {
	
	private BtCandidateGenerator generator;

	@Before
	public void setUp() throws Exception {
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(5);
		availablePieces.put(PieceTypes.BISHOP, 1);
		availablePieces.put(PieceTypes.KING, 1);
		availablePieces.put(PieceTypes.KNIGHT, 0);
		availablePieces.put(PieceTypes.QUEEN, 0);
		availablePieces.put(PieceTypes.ROOK, 0);
		
		// Use a 2(rows)x2(columns) board for testing
		ChessProblem problem = new ChessProblem(availablePieces, 2, 2);
		BtNode node = new BtNode(problem);
		BtChessSolver solver = new BtChessSolver();
		generator = new BtCandidateGenerator(node, solver);
	}

	@Test
	public void testHasNext() {
		assertTrue(generator.hasNext());
	}

	@Test
	public void testNext() {
		BtCandidate candidate = generator.next();
		assertEquals(0, candidate.getIndex());
		assertEquals(PieceTypes.BISHOP, candidate.getPieceType());
	}

	@Test
	public void testIteration() {
		int index = 0;
		BtCandidate[] candidates = new BtCandidate[4];
		
		// Generating candidates will give only 4 as result as in a 2x2 board
		// all positions are symmetric.
		while(generator.hasNext()) {
			BtCandidate candidate = generator.next();
			assertNotNull(candidate);
			if (index > 1) {
				assertTrue(
						(candidates[index-1].getIndex() < candidate.getIndex()
								&& candidates[index-1].getPieceType() == candidate.getPieceType()));
			}
			candidates[index++] = candidate;
		}
		
		assertEquals(1, index);
	}
	
	@Test
	public void testIteration_4x4_repeated_pieces() {
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(5);
		availablePieces.put(PieceTypes.BISHOP, 0);
		availablePieces.put(PieceTypes.KING, 0);
		availablePieces.put(PieceTypes.KNIGHT, 4);
		availablePieces.put(PieceTypes.QUEEN, 0);
		availablePieces.put(PieceTypes.ROOK, 2);
		
		ChessProblem problem = new ChessProblem(availablePieces, 5, 5);
		BtNode node = new BtNode(problem);
		BtChessSolver solver = new BtChessSolver();
		BtCandidateGenerator generator4x4 = new BtCandidateGenerator(node, solver, PieceTypes.KNIGHT);
		int index = 0;
		// There are 4x4 candidates in each step at maximum. some of them will
		// not be returned as being symmetric with previous ones
		BtCandidate[] candidates = new BtCandidate[4*4];
		
		while(generator4x4.hasNext()) {
			BtCandidate candidate = generator4x4.next();
			assertNotNull(candidate);
			if (index > 1) {
				assertTrue(
						(candidates[index-1].getIndex() < candidate.getIndex()
								&& candidates[index-1].getPieceType() == candidate.getPieceType()));
			}
			candidates[index++] = candidate;
		}
		
		assertEquals(6, index);
	}
}
