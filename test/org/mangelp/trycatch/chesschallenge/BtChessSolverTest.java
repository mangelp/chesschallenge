package org.mangelp.trycatch.chesschallenge;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class BtChessSolverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSolve_3x2_1K_1R() {
		BtChessSolver solver = new BtChessSolver();
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(3);
		availablePieces.put(PieceTypes.KING, 2);
		availablePieces.put(PieceTypes.ROOK, 1);
		ChessProblem problem = new ChessProblem(availablePieces, 3, 3);
		
		solver.setPrintSolutions(true);
		solver.setStoreSolutions(true);
		solver.solve(problem);
		
		assertEquals(4, solver.getUniqueSolutions());
		assertEquals(4, solver.getSolutions().size());
	}

	@Test
	public void testSolve_4x4_2R_4N() {
		BtChessSolver solver = new BtChessSolver();
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(3);
		availablePieces.put(PieceTypes.ROOK, 2);
		availablePieces.put(PieceTypes.KNIGHT, 4);
		ChessProblem problem = new ChessProblem(availablePieces, 4, 4);
		
		solver.setPrintSolutions(true);
		solver.setStoreSolutions(true);
		solver.solve(problem);
		
		assertEquals(8, solver.getUniqueSolutions());
		assertEquals(8, solver.getSolutions().size());
	}
	
	@Test
	public void testSolve_7x7_2K_2Q_2B_1N() {
		
		BtChessSolver solver = new BtChessSolver();
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(3);
		availablePieces.put(PieceTypes.KING, 2);
		availablePieces.put(PieceTypes.KNIGHT, 1);
		availablePieces.put(PieceTypes.BISHOP, 2);
		availablePieces.put(PieceTypes.QUEEN, 2);
		
		ChessProblem problem = new ChessProblem(availablePieces, 7, 7);
		solver.setPrintSolutions(false);
		solver.setStoreSolutions(true);
		solver.solve(problem);
		
		assertEquals(3063828, solver.getUniqueSolutions());
		
		List<BtSolution> solutions = solver.getSolutions();
		assertEquals(3063828, solutions.size());
	}
}
