package org.mangelp.trycatch.chesschallenge;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class ChessBoardHelperTest {
	
	private ChessBoardHelper helper;

	@Before
	public void setUp() throws Exception {
		HashMap<PieceTypes, Integer> availablePieces = new HashMap<PieceTypes, Integer>(5);
		availablePieces.put(PieceTypes.BISHOP, 2);
		availablePieces.put(PieceTypes.KING, 1);
		availablePieces.put(PieceTypes.KNIGHT, 0);
		availablePieces.put(PieceTypes.QUEEN, 0);
		availablePieces.put(PieceTypes.ROOK, 0);
		
		// Use a 3(w)x4(h) board for testing
		ChessProblem problem = new ChessProblem(availablePieces, 3, 4);
		helper = new ChessBoardHelper(problem);
	}

	@Test
	public void testGetIndexPositionXAndYAndPositionFromIndex() {
		
		int width = helper.getBoardWidth();
		
		for (int index=0; index < width; index++) {
			int x = helper.getIndexPositionX(index);
			assertEquals(0, x);
			int y = helper.getIndexPositionY(index);
			assertEquals(index, y);
		}
		
		for (int index=width; index < width*2; index++) {
			int x = helper.getIndexPositionX(index);
			assertEquals(1, x);
			int y = helper.getIndexPositionY(index);
			assertEquals(index-width, y);
		}
		
		int totalLength = helper.getBoardHeight()
				* helper.getBoardWidth();
		
		for (int index=0; index<totalLength; index++) {
			int x = helper.getIndexPositionX(index);
			int y = helper.getIndexPositionY(index);
			int index2 = helper.getPositionIndex(x, y);
			
			assertEquals(index, index2);
		}
	}

	@Test
	public void testIsPositionInDiagonalALL() {
		boolean[][] diagonals = new boolean[][]{
			{true, false, false},
			{false, true, false},
			{false, false, true},
			{false, false, false},
		};
		
		int x = 0;
		int y = 0;
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(diagonals[i][j], 
						helper.isPositionInDiagonal(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(diagonals[i][j], 
						helper.isPositionInDiagonal(index, posIndex));
			}
		}
		
		x = 2;
		y = 1;
		index = 2*helper.getBoardWidth() + y;
		
		diagonals = new boolean[][]{
				{false, false, false},
				{true, false, true},
				{false, true, false},
				{true, false, true},
			};
			
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(diagonals[i][j], 
						helper.isPositionInDiagonal(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(diagonals[i][j], 
						helper.isPositionInDiagonal(index, posIndex));				
			}
		}
	}

	@Test
	public void testIsPositionInLineALL() {
		boolean[][] lines = new boolean[][]{
			{true, true, true},
			{true, false, false},
			{true, false, false},
			{true, false, false},
		};
		
		int x = 0;
		int y = 0;
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(lines[i][j], 
						helper.isPositionInLine(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(lines[i][j], 
						helper.isPositionInLine(index, posIndex));
			}
		}
		
		x = 2;
		y = 1;
		index = 2*helper.getBoardWidth() + y;
		
		lines = new boolean[][]{
				{false, true, false},
				{false, true, false},
				{true, true, true},
				{false, true, false},
			};
			
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(lines[i][j], 
						helper.isPositionInLine(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(lines[i][j], 
						helper.isPositionInLine(index, posIndex));				
			}
		}
	}

	@Test
	public void testIsPositionInJumpALL() {
		boolean[][] jumps = new boolean[][]{
			{false, false, false},
			{false, false, true},
			{false, true, false},
			{false, false, false},
		};
		
		int x = 0;
		int y = 0;
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(jumps[i][j], 
						helper.isPositionInJump(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(jumps[i][j], 
						helper.isPositionInJump(index, posIndex));
			}
		}
		
		x = 2;
		y = 1;
		index = 2*helper.getBoardWidth() + y;
		
		jumps = new boolean[][]{
				{true, false, true},
				{false, false, false},
				{false, false, false},
				{false, false, false},
			};
			
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(jumps[i][j], 
						helper.isPositionInJump(x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(jumps[i][j], 
						helper.isPositionInJump(index, posIndex));				
			}
		}
	}

	@Test
	public void testIsPositionAtDistanceAll() {
		boolean[][] distances = new boolean[][]{
			{false, true, false},
			{true, true, false},
			{false, false, false},
			{false, false, false},
		};
		
		int x = 0;
		int y = 0;
		int index = 0;
		int distance = 1;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(distances[i][j], 
						helper.isPositionAtDistance(distance, x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(distances[i][j], 
						helper.isPositionAtDistance(distance, index, posIndex));
			}
		}
		
		x = 2;
		y = 1;
		index = 2*helper.getBoardWidth() + y;
		
		distances = new boolean[][]{
				{false, false, false},
				{true, true, true},
				{true, false, true},
				{true, true, true},
			};
			
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				assertEquals(distances[i][j], 
						helper.isPositionAtDistance(distance, x,  y, i, j));
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(distances[i][j], 
						helper.isPositionAtDistance(distance, index, posIndex));				
			}
		}
	}

	@Test
	public void testCanPieceTakeOut() {
		
	}

	@Test
	public void testCanBishopTakeOut() {
		boolean[][] expected = new boolean[][]{
			{false, false, false},
			{false, true, false},
			{false, false, true},
			{false, false, false},
		};
		
		// Bishop at (0,0)
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canBishopTakeOut(index, posIndex));
			}
		}
		
		expected = new boolean[][]{
			{true, false, true},
			{false, false, false},
			{true, false, true},
			{false, false, false},
		};
		
		// Bishop at (1,1)
		index = 4;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canBishopTakeOut(index, posIndex));
			}
		}
	}

	@Test
	public void testCanKingTakeOut() {
		boolean[][] expected = new boolean[][]{
			{false, true, false},
			{true, true, false},
			{false, false, false},
			{false, false, false},
		};
		
		// King at (0,0)
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canKingTakeOut(index, posIndex));
			}
		}
		
		expected = new boolean[][]{
			{true, true, true},
			{true, false, true},
			{true, true, true},
			{false, false, false},
		};
		
		// King at (1,1)
		index = 4;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canKingTakeOut(index, posIndex));
			}
		}
	}

	@Test
	public void testCanKnightTakeOut() {
		boolean[][] expected = new boolean[][]{
			{false, false, false},
			{false, false, true},
			{false, true, false},
			{false, false, false},
		};
		
		// Knight at (0,0)
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canKnightTakeOut(index, posIndex));
			}
		}
		
		expected = new boolean[][]{
			{false, false, false},
			{false, false, false},
			{false, false, false},
			{true, false, true},
		};
		
		// Knight at (1,1)
		index = 4;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canKnightTakeOut(index, posIndex));
			}
		}
	}

	@Test
	public void testCanQueenTakeOut() {
		boolean[][] expected = new boolean[][]{
			{false, true, true},
			{true, true, false},
			{true, false, true},
			{true, false, false},
		};
		
		// Queen at (0,0)
		int index = 0;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canQueenTakeOut(index, posIndex));
			}
		}
		
		expected = new boolean[][]{
			{true, true, true},
			{true, false, true},
			{true, true, true},
			{false, true, false},
		};
		
		// Queen at (1,1)
		index = 4;
		
		for(int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				int posIndex = helper.getPositionIndex(i, j);
				assertEquals(expected[i][j], 
						helper.canQueenTakeOut(index, posIndex));
			}
		}
	}

	@Test
	public void testCanRookTakeOut() {
		boolean[][] expected = new boolean[][]{
				{false, true, true},
				{true, false, false},
				{true, false, false},
				{true, false, false},
			};
			
			// Rook at (0,0)
			int index = 0;
			
			for(int i=0; i<3; i++) {
				for (int j=0; j<3; j++) {
					int posIndex = helper.getPositionIndex(i, j);
					assertEquals(expected[i][j], 
							helper.canRookTakeOut(index, posIndex));
				}
			}
			
			expected = new boolean[][]{
				{false, true, false},
				{true, false, true},
				{false, true, false},
				{false, true, false},
			};
			
			// Rook at (1,1)
			index = 4;
			
			for(int i=0; i<3; i++) {
				for (int j=0; j<3; j++) {
					int posIndex = helper.getPositionIndex(i, j);
					assertEquals(expected[i][j], 
							helper.canRookTakeOut(index, posIndex));
				}
			}
	}

}
