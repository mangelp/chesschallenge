package org.mangelp.trycatch.chesschallenge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Resolution of the chess problem using backtracking.
 * @author Miguel Angel Perez
 *
 */
public class BtChessSolver {
	
	private ChessBoardHelper helper;
	
	private ChessProblem problem;
	
	private Map<String, BtSolution> solutions;
	
	private long startTime = 0L;
	
	private long lastSolutionTime = 0L;
	
	private long uniqueSolutions = 0L;
	
	private long duplicatedSolutions = 0L;
	
	private long endTime = 0L;
	
	private boolean excludeSymmetricFromCandidates = false;
	
	private boolean storeSolutions = false;
	
	private boolean printSolutions = false;
	
	public long getDuplicatedSolutions() {
		return duplicatedSolutions;
	}
	
	public long getUniqueSolutions() {
		return uniqueSolutions;
	}
	
	public long getLastSolutionTime() {
		return lastSolutionTime;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public ChessBoardHelper getHelper() {
		return helper;
	}
	
	public ChessProblem getProblem() {
		return problem;
	}
	
	public boolean isExcludeSymmetricFromCandidates() {
		return excludeSymmetricFromCandidates;
	}
	
	public void setExcludeSymmetricFromCandidates(
			boolean excludeSymmetricFromCandidates) {
		this.excludeSymmetricFromCandidates = excludeSymmetricFromCandidates;
	}
	
	public boolean isPrintSolutions() {
		return printSolutions;
	}
	
	public void setPrintSolutions(boolean printSolutions) {
		this.printSolutions = printSolutions;
	}
	
	public boolean isStoreSolutions() {
		return storeSolutions;
	}
	
	public void setStoreSolutions(boolean storeSolutions) {
		this.storeSolutions = storeSolutions;
	}
	
	public List<BtSolution> getSolutions() {
		ArrayList<BtSolution> solutionsList = new ArrayList<BtSolution>(solutions.size() + 2);
		solutionsList.addAll(solutions.values());
		return solutionsList;
	}
	
	/**
	 * Solve the given problem using backtracking
	 * @param problem
	 */
	public void solve(ChessProblem problem) {
		this.startTime = System.currentTimeMillis();
		this.lastSolutionTime = System.currentTimeMillis();
		
		BtNode node = createRoot(problem);
		this.helper = new ChessBoardHelper(problem);
		this.problem = problem;
		// Store solutions to avoid duplicates
		int initialCapacity = problem.getBoardHeight() 
				* problem.getBoardWidth() 
				* problem.getTotalPieces() 
				* problem.getTotalPieces();
		this.solutions = new HashMap<String, BtSolution>(initialCapacity);
		
		// This is the call to the recursive method.
		// This method is intended to find a part of the
		// solutions trying to cut down on symmetric solutions
		// but after it ends we should ensure that we add
		// all symmetric solutions to have a complete
		// result
		this.btSolve(node);
		
		this.endTime = System.currentTimeMillis();
	}

	private BtNode createRoot(ChessProblem problem) {
		BtNode node = new BtNode(problem);
		
		return node;
	}

	public void btSolve(BtNode node) {
		if (isSolution(node)) {
			addSolution(node);
		} else {
			BtCandidateGenerator candidates = getCandidates(node);
			BtNode nextNode = node.getNextNodeCopy();
			
			while(candidates.hasNext()){
				BtCandidate candidate = candidates.next();
				
				if (isPromising(node, candidate)) {
					addCandidateToNode(nextNode, candidate, candidates);
					btSolve(nextNode);
					removeCandidateFromNode(nextNode, candidate, candidates);
				}
			}
		}
	}

	private void addCandidateToNode(BtNode node, BtCandidate candidate, BtCandidateGenerator candidates) {
		node.addPieceAtPosition(candidate.getPieceType(), candidate.getIndex());
		node.setNumSolutions(this.uniqueSolutions + this.duplicatedSolutions);
	}
	
	private void removeCandidateFromNode(BtNode node, BtCandidate candidate, BtCandidateGenerator candidates) {
		node.removeLastAddedPiece();
		
		if (node.getNumSolutions() < (this.uniqueSolutions + this.duplicatedSolutions)) {
			candidates.candidateProducedSolutions(candidate);
		}
	}

	/**
	 * Checks if adding the candidate to the given node (partial solution) seems
	 * good enough to go ahead.
	 * 
	 * A candidate is promising if the node has no pieces added (can't threaten anything)
	 * or if the candidate piece doesn't threatens any other piece and is set to be
	 * placed in an empty position.
	 * 
	 * @param nextState
	 * @param candidate 
	 * @return true if is promising to add the candidate to the node or false
	 * if not.
	 */
	public boolean isPromising(BtNode node, BtCandidate candidate) {
		if (node.getAddedPiecesCount() == 0) {
			return true;
		}
		
		if (node.getBoard()[candidate.getIndex()] != null) {
			return false;
		}
		
		for(int index = 0; index<node.getBoard().length; index++) {
			PieceTypes type = node.getBoard()[index];
			
			if (type == null) {
				continue;
			}
			
			// Check if the candidate can take out other pieces
			boolean canTakeOut = this.helper.canPieceTakeOut(
					candidate.getPieceType(), 
					candidate.getIndex(), 
					index);
			
			if (canTakeOut) {
				return false;
			}
			
			// Check if the other piece can take out the candidate
			canTakeOut = this.helper.canPieceTakeOut(
					type, 
					index,
					candidate.getIndex());
			
			if (canTakeOut) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Returns a candidate generator that generates candidates given a current 
	 * partial solution state.
	 * 
	 * @param node
	 * @return
	 */
	public BtCandidateGenerator getCandidates(BtNode node) {
		BtCandidateGenerator generator = new BtCandidateGenerator(node, this);
		
		return generator;
	}
	
	/**
	 * Checks if the given node is a solution for the problem.
	 * 
	 * A node (a partial solution) is a solution if there are no pieces left
	 * to be added. This is enough as the solve() method always checks first
	 * that the last added piece doesn't threatens any other piece so when
	 * all pieces are placed, we have a solution.
	 * 
	 * @param node
	 * @return
	 */
	public boolean isSolution(BtNode node) {
		return node.getAddedPiecesCount() == this.getProblem().getTotalPieces();
	}
	
	protected void printProgress() {
		long dif = System.currentTimeMillis() 
				- this.lastSolutionTime;
		
		System.out.println(String.format(
				"\tTime: last solution %d ms, total %d ms", dif, System.currentTimeMillis() - startTime));
		System.out.println(String.format(
				"\tSolutions: %d unique, %d duplicated", uniqueSolutions, duplicatedSolutions));
	}

	/**
	 * Adds a solution to the list of solutions found used to not print them twice
	 * 
	 * @param node
	 * @return true if the solution is added (unique) or false if not (repeated).
	 */
	public boolean addSolution(BtNode node) {
		
		BtSolution solution = new BtSolution(node.getBoard());
		this.lastSolutionTime = System.currentTimeMillis();
		
		if (this.printSolutions) {
			solution.print(problem, this);
		}
		
		if (!this.storeSolutions) {
			// If we are not storing solutions all solutions are unique as we
			// can't check it
			this.uniqueSolutions++;
			return true;
		}
		
		if (this.solutions.containsKey(solution.getHash())) {
			// Don't add duplicated solutions
			this.duplicatedSolutions++;
			return false;
		}
		
		this.solutions.put(solution.getHash(), solution);
		this.uniqueSolutions++;
		
		if (this.excludeSymmetricFromCandidates) {
			// Add symmetric solutions
			int numSymmetricSolutions = this.helper.generateSymmetricsSolutions(solution, solutions);
			this.uniqueSolutions+= numSymmetricSolutions;
		}
		
		return true;
	}
}
