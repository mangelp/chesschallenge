package org.mangelp.trycatch.chesschallenge;

import java.util.HashMap;

public class ChessChallenge {

	public static void main(String[] args) {
		
		if (args.length < 2 || args.length > 3) {
			System.out.println("Usage: java -jar APP.jar WIDTH PIECES [--print-solutions]");
			System.out.println("  Where PIECES is a comma separated list of PIECE@NUMBER");
			return;
		}
		
		int width = 0;
		int height = 0;
		HashMap<PieceTypes, Integer> problemPieces = 
				new HashMap<PieceTypes, Integer>();
		
		try {
			width = Integer.parseInt(args[0]);
			height = width;
			String pieces = args[1];
			
			String[] pieceParts = pieces.split(",");
			
			for(String part : pieceParts) {
				int separatorPos = part.indexOf('@');
				int number = Integer.parseInt(part.substring(separatorPos + 1));
				PieceTypes type = PieceTypes.fromString(part.substring(0, separatorPos));
				
				problemPieces.put(type, number);
			}
		}
		catch (Throwable t) {
			System.out.println(
					"Arguments error: " + t.getClass().getName() 
					+ ": " + t.getMessage());
			return;
		}
		
		ChessProblem problem = new ChessProblem(problemPieces, width, height);
		BtChessSolver solver = new BtChessSolver();
		
		boolean printSolutions = args.length == 3 && args[2].equals("--print-solutions");
		
		solver.setPrintSolutions(printSolutions);
		solver.setStoreSolutions(false);
		solver.solve(problem);
		
		System.out.println(" * Unique solutions: " + solver.getUniqueSolutions());
		
		if (solver.isStoreSolutions() && solver.getDuplicatedSolutions() != 0) {
			System.out.println(" * Duplicated solutions: " + solver.getDuplicatedSolutions());
		}
		
		System.out.println(" * Time spent: " + (solver.getEndTime() - solver.getStartTime()) + " ms");
	}
}
