package org.mangelp.trycatch.chesschallenge;

import java.util.HashMap;
import java.util.Map;

/**
 * Models all the problem details
 * @author Miguel Angel Perez
 *
 */
public class ChessProblem {
	/**
	 * Map of available number of pieces by each piece type
	 */
	private HashMap<PieceTypes, Integer> availablePieces;
	/**
	 * Board width in squares
	 */
	private int boardWidth;
	/**
	 * Board height in squares
	 */
	private int boardHeight;
	
	/**
	 * Total count of available pieces of all types.
	 */
	private int totalPieces;
	
	public int getBoardWidth() {
		return boardWidth;
	}
	
	public int getBoardHeight() {
		return boardHeight;
	}
	
	public int getTotalPieces() {
		return totalPieces;
	}
	
	public HashMap<PieceTypes, Integer> getAvailablePieces() {
		return availablePieces;
	}
	
	public ChessProblem(Map<PieceTypes, Integer> availablePieces, int boardWidth, int boardHeight) {
		this.availablePieces = new HashMap<PieceTypes, Integer>(availablePieces);
		this.boardHeight = boardHeight;
		this.boardWidth = boardWidth;
		this.totalPieces = 0;
		
		for(PieceTypes type: availablePieces.keySet()) {
			this.totalPieces += availablePieces.get(type);
		}
	}
}
