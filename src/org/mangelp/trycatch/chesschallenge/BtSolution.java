package org.mangelp.trycatch.chesschallenge;

import com.bethecoder.ascii_table.ASCIITable;

public class BtSolution {
	private PieceTypes[] board;
	private String hash;
	
	public PieceTypes[] getBoard() {
		return board;
	}
	
	public String getHash() {
		return hash;
	}
	
	public BtSolution(PieceTypes[] board) {
		this.board = new PieceTypes[board.length];
		StringBuilder sb = new StringBuilder(board.length);
		
		for(int i=0; i<board.length; i++) {
			this.board[i] = board[i];
			if (board[i] != null) {
				sb.append(i + ":" + board[i] + ",");
			}
		}
		
		hash = sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null 
				|| !obj.getClass().equals(this.getClass())) {
			
			return false;
		}
		
		return this.hash.equals(
				((BtSolution)obj).hash);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Solution:");
		
		for(int i=0; i<this.board.length; i++) {
			if (board[i] != null) {
				sb.append(board[i] + "@" + i + ",");
			}
		}
		
		return sb.toString();
	}
	
	public void print(ChessProblem problem, BtChessSolver solver) {
		String[] header = new String[0];
		String[][] data = new String[problem.getBoardHeight()][];
		
		for (int i = 0; i< problem.getBoardHeight(); i++) {
			data[i] = new String[problem.getBoardWidth()];
			
			for (int j=0 ; j<problem.getBoardWidth(); j++) {
				int index = solver.getHelper().getPositionIndex(i, j);
				PieceTypes type = this.board[index];
				if (type != null) {
					data[i][j] = String.format("%s", type.getAbreviation());
				}
				else {
					data[i][j] = " ";
				}
			}
		}
		
		ASCIITable.getInstance().printTable(header, data);
	}
}
