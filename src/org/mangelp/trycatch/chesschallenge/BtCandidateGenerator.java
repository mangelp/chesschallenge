package org.mangelp.trycatch.chesschallenge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Backtracking candidate generator. Generate all candidates from a given node 
 * state using all remaining pieces and positions.
 * 
 * It implements the Iterator<BtCandidate> interface.
 * 
 * At first glance the candidates generated are all the combinations of 
 * positions on the board for each one of the remaining piece types. That means
 * N*M*P candidates to check for each node where N is board rows, M is board 
 * columns and P is the total number of pieces. 
 * 
 * But some of them will not be viable and for a 4x4 board with three pieces we
 * get to test 48 candidates in each step. If we implement this the first thing
 * we notice is that there are a lot of duplicated solutions. Different candidate
 * choices take us to the same solutions. This is due to the chess board being 
 * squared and to having more than one piece of each type. Then is important to
 * try to filter candidates that might give duplicated solutions.
 * 
 * First we must not generate for the same node candidates for the same piece
 * type twice, that duplicates automatically the number of solutions that are
 * reachable by placing that piece type in each board position as the 
 * backtracking algorithm is going to use all candidates one by one and don't 
 * check for duplicates because it doesn't stores them to save memory. This 
 * reduces candidates from N*M*P to N*M*T where T is the number of different 
 * piece types to place. A 4x4 board with two kings and one Rook produce 32 
 * candidates per node.
 * 
 * Next we find that when we generate all combinations of piece types and
 * positions we are testing combinations where the order of the added pieces
 * to the board should matter (put first a king, then a rook, go back put first
 * the rook, then put the king, ...). But is not the case, in this problem the
 * order in which the pieces are placed doesn't matters and a solution with a+
 * king in one position and a rook into another is the same no matter the order
 * used to place the pieces. So in each step we won't generate candidates for
 * all available piece types, and we will be only generating candidates for one
 * of them, just taking the first available.
 * 
 * Next we find that the chess board, being squared, can be rotated and 
 * reflected, so any solution can be rotated and reflected and we will get new
 * valid solutions.
 * Then if we cut down nodes whose solutions are symmetrical we can generate
 * them easily later, as we only have to rotate and reflect current solutions.
 * But we only do it between the candidates of a single step. For example 
 * when placing a piece in step N to get to step N+1, if that step leads to a
 * solution then we won't use any other position that is symmetrical to the one
 * that got a solution. This won't eliminate all symmetrical solutions only 
 * those that begin by putting a given piece type over the board. For example 
 * for a 3x3 board with 2 kings and 1 rook placing a king at any of the corners 
 * gives us a solution and all of them are symmetric by rotating any of them.
 * 
 * Last we will handle duplicated solutions due to multiple pieces of the same
 * types. For example putting a king at 0,0 and a king at 0,2 is the same as
 * putting the first at 0,2 and then the second at 0,0. Then we restrict, for
 * pieces placed with the same type as the one placed before, to indexes 
 * greater than the index used in the last placement.
 * 
 * @author Miguel Angel Perez
 *
 */
public class BtCandidateGenerator implements Iterator<BtCandidate> {
	
	/**
	 * Reference to the backtracking node used to initialize this candidate
	 */
	private BtNode currentNode;
	
	/**
	 * PieceTypes[] to use for candidates
	 */
	private PieceTypes pieceType;
	
	/**
	 * Index to iterate all board positions and generate a placement of each
	 * piece type on in.
	 */
	private int currentIndex = -1;
	
	/**
	 * Total number of squares on the board (rows x columns).
	 */
	private int boardSize = 0;
	
	/**
	 * Next viable candidate for index
	 */
	private int nextIndex = -1;
	private boolean nextIndexCalculated = false;
	
	private List<Integer> positionsWithSolutions = new ArrayList<Integer>();
	
	public BtNode getCurrentNode() {
		return currentNode;
	}
	
	private ChessBoardHelper helper;

	private BtChessSolver solver;
	
	public BtChessSolver getSolver() {
		return solver;
	}
	
	/**
	 * Initialize all fields using the current node instance passed
	 * @param currentNode
	 */
	public BtCandidateGenerator(BtNode currentNode, BtChessSolver solver) {
		this(currentNode, solver, null);
	}
	
	/**
	 * Initialize all fields using the current node instance passed
	 * @param currentNode
	 */
	public BtCandidateGenerator(BtNode currentNode, BtChessSolver solver, PieceTypes type) {
		this.currentNode = currentNode;
		this.helper = solver.getHelper();
		this.solver = solver;
		this.pieceType = type;
		this.boardSize = helper.getBoardWidth() * helper.getBoardHeight();
		
		if (type == null) {
			// Find an available piece type.
			
			for (PieceTypes pieceType: currentNode.getAvailablePieces().keySet()) {
				if (currentNode.getAvailablePieces().get(pieceType) > 0) {
					// Whenever we find an available piece type we set it and stop
					// searching, as we only need one piece type.
					this.pieceType = pieceType;
				}
			}
		}
	}
	
	/**
	 * Gets the board index to the next viable position checking position symmetry
	 * as at any step generating candidates for symmetric positions will take us
	 * to the same solutions and we want to cut down that to cut down some branches.
	 * 
	 * @return the next position index or -1 if there are no next position index
	 */
	protected int findNextIndex() {
		int nextIndex = currentIndex;
		boolean found = false;
		
		// If the previous node piece is the same as the one being used as
		// candidate then to avoid duplication we must not try to put it
		// into any index lesser than the one used by it in the previous
		// step.
		if (currentNode.getLastPieceType() == pieceType 
				&& currentNode.getLastIndex() > nextIndex) {
			
			// Assign it, it is used but the loop increments it so we
			// will begin by the next one.
			nextIndex = currentNode.getLastIndex();
		}
		
		while(!found) {
			nextIndex++;
			
			// Avoid getting out of bounds
			if (nextIndex >= this.boardSize) {
				break;
			}
			
			// Skip used positions
			if (currentNode.getBoard()[nextIndex] != null) {
				continue;
			}
			
			// Assume nextIndex is ok, try to probe is not
			found = true;
			
			// 0 is always good if not used
			if (nextIndex == 0) {
				break;
			}
			
			if (this.solver.isExcludeSymmetricFromCandidates()) {
				// Iterate previous positions that generated solutions and avoid
				// new positions that are symmetric to them
				for (int i: positionsWithSolutions) {
					if (helper.isPositionSymmetric(nextIndex, i)) {
						
						found = false;
						break;
					}
				}
			}
		}
		
		if (!found) {
			return -1;
		}
		
		return nextIndex;
	}

	@Override
	public boolean hasNext() {
		if (!nextIndexCalculated) {
			nextIndex = this.findNextIndex();
			nextIndexCalculated = true;
		}
		
		return nextIndex < this.boardSize && nextIndex != -1;
	}

	@Override
	public BtCandidate next() {
		if (currentIndex >= this.boardSize 
				|| (nextIndexCalculated && nextIndex == -1)) {
			return null;
		}
		
		if (!nextIndexCalculated) {
			nextIndex = this.findNextIndex();
			nextIndexCalculated = true;
		}
		
		currentIndex = nextIndex;
		
		BtCandidate candidate = new BtCandidate(this.pieceType, currentIndex);
		
		// Next iteration indexes. We will be iterating first by piece type, and then by board index
		nextIndexCalculated = false;
		nextIndex = -1;

		return candidate;
	}

	/**
	 * After using this candidate (a promising one) one or more solutions
	 * where found.
	 * We will use this information to avoid placing the current piece in any
	 * position symmetric to the one it provided a solution.
	 * @param candidate
	 */
	public void candidateProducedSolutions(BtCandidate candidate) {
		this.positionsWithSolutions.add(candidate.getIndex());
	}
}
