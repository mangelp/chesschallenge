package org.mangelp.trycatch.chesschallenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

/**
 * Helper that implement utility methods to handle positions, helper methods
 * to check if a given piece type threatens a position, symmetric positions
 * checks and symmetric solution generation. 
 * 
 * @author Miguel Angel Perez
 *
 */
public class ChessBoardHelper {
	
	public enum SymmetryOperations {
		ROTATE,
		REFLECT,
	}
	
	private int boardWidth;
	
	private int boardHeight;
	
	private ArrayList<int[]> indexSymmetryCache;
	
	public int getBoardHeight() {
		return boardHeight;
	}
	
	public int getBoardWidth() {
		return boardWidth;
	}
	
	public ChessBoardHelper(ChessProblem problem) {
		this.boardHeight = problem.getBoardHeight();
		this.boardWidth = problem.getBoardWidth();
		
		this.indexSymmetryCache = new ArrayList<int[]>();
		this.preCalculateIndexSymmetries();
	}
	
	protected void preCalculateIndexSymmetries() {
		int size = this.boardHeight * this.boardWidth;
		
		for (int index=0; index<size; index++) {
			// Calculate rotations and for each rotation his reflection
			ArrayList<Integer> symmetricPoints = new ArrayList<Integer>();
			int row = this.getIndexPositionX(index);
			int column = this.getIndexPositionY(index);
			
			int rotatedRow = 0;
			int rotatedColumn = 0;
			int reflectedRow = row;
			int reflectedColumn = this.boardWidth - 1 - column;
			int symmetricIndex = this.getPositionIndex(reflectedRow, reflectedColumn);
			if (symmetricIndex != index && !symmetricPoints.contains(symmetricIndex)) {
				symmetricPoints.add(symmetricIndex);
			}
			
			for (int j=0; j<6; j+=2) {
				rotatedRow = column;
				rotatedColumn = this.boardWidth - 1 - row;
				
				symmetricIndex = this.getPositionIndex(rotatedRow, rotatedColumn);
				if (symmetricIndex != index && !symmetricPoints.contains(symmetricIndex)) {
					symmetricPoints.add(symmetricIndex);
				}
				
				reflectedRow = rotatedRow;
				reflectedColumn = this.boardWidth - 1 - rotatedColumn;
				symmetricIndex = this.getPositionIndex(reflectedRow, reflectedColumn);
				if (symmetricIndex != index && !symmetricPoints.contains(symmetricIndex)) {
					symmetricPoints.add(symmetricIndex);
				}
				
				row = rotatedRow;
				column = rotatedColumn;
			}
			
			symmetricPoints.sort(new Comparator<Integer>() {

				@Override
				public int compare(Integer o1, Integer o2) {
					
					return o1 - o2;
				}
			});
			
			int[] tmp = new int[symmetricPoints.size()];
			for (int i=0; i<tmp.length; i++) {
				tmp[i] = symmetricPoints.get(i);
			}
			this.indexSymmetryCache.add(tmp);
		}
	}
	
	/**
	 * Calculates the X position for a given index
	 * @param index
	 * @return
	 */
	public int getIndexPositionX(int index) {
		return index / this.boardWidth;
	}
	
	/**
	 * Calculates the Y position for a given index
	 * @param index
	 * @return
	 */
	public int getIndexPositionY(int index) {
		return index % this.boardWidth;
	}
	
	/**
	 * Calculates the index of the array for a given position
	 * @param x Row
	 * @param y Column
	 * @return
	 */
	public int getPositionIndex(int x, int y) {
		return x*this.boardWidth + y;
	}
	
	/**
	 * Checks if two given positions are in the diagonal of each other
	 * 
	 * Position (x2,y2) in in the diagonal of (x1,y1) if the difference of
     * X coordinates is the same as the difference of Y coordinates without
	 * sign.
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return boolean true if (x1,y1) is in diagonal with (x2,y2)
	 */
	public boolean isPositionInDiagonal(int x1, int y1, int x2, int y2) {
		// Point (x2,y2) in in the diagonal of (x1,y1) if the difference of
		// x coordinates is the same as the difference of y coordinates without
		// sign.
		
		return Math.abs(x1 - x2) == Math.abs(y1 - y2);
	}
	
	/**
	 * Checks if two indexes of an array that represent a matrix are in the
	 * diagonal of each other.
	 * 
	 * We will convert from index position to (x,y) coordinates and call the
	 * proper coordinate-based method.
	 * 
	 * @param index1
	 * @param index2
	 * @return boolean
	 */
	public boolean isPositionInDiagonal(int index1, int index2) {
		int x1 = this.getIndexPositionX(index1);
		int y1 = this.getIndexPositionY(index1);
		int x2 = this.getIndexPositionX(index2);
		int y2 = this.getIndexPositionY(index2);
		
		return this.isPositionInDiagonal(x1, y1, x2, y2);
	}
	
	/**
	 * Checks if two given positions are in the same x or y coordinate
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return boolean true if (x1,y1) is in diagonal with (x2,y2)
	 */
	public boolean isPositionInLine(int x1, int y1, int x2, int y2) {
		// Point (x2,y2) in in the diagonal of (x1,y1) if the difference of
		// x coordinates is the same as the difference of y coordinates without
		// sign.
		
		return x1 == x2 || y1 == y2;
	}
	
	/**
	 * Checks if two given indexes are in the same x or y coordinates.
	 * 
	 * Convert them to coordinates and call the position-based method
	 * 
	 * @param index1
	 * @param index2
	 * 
	 * @return boolean true if (x1,y1) is in diagonal with (x2,y2)
	 */
	public boolean isPositionInLine(int index1, int index2) {
		int x1 = this.getIndexPositionX(index1);
		int y1 = this.getIndexPositionY(index1);
		int x2 = this.getIndexPositionX(index2);
		int y2 = this.getIndexPositionY(index2);
		
		return this.isPositionInLine(x1, y1, x2, y2);
	}
	
	/**
	 * Checks if you can move from one position to another jumping like a
	 * chess horse.
	 * 
	 * Chess horse moves 2 squares in one coordinate and then another one
	 * in the other coordinate. So the jump can be done if the difference
	 * between X or Y coordinates is 2 and the difference of the other
	 * coordinate is 1, without signs.
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return boolean true if (x1,y1) is in diagonal with (x2,y2)
	 */
	public boolean isPositionInJump(int x1, int y1, int x2, int y2) {

		return (Math.abs(x1 - x2) == 2 && Math.abs(y1 - y2) == 1)
				|| (Math.abs(x1 - x2) == 1 && Math.abs(y1 - y2) == 2);
	}
	
	/**
	 * Checks if you can move from one position to another jumping like a
	 * chess horse.
	 * 
	 * Convert them to coordinates and call the position-based method
	 * 
	 * @param index1
	 * @param index2
	 * 
	 * @return boolean true if (x1,y1) is in diagonal with (x2,y2)
	 */
	public boolean isPositionInJump(int index1, int index2) {
		int x1 = this.getIndexPositionX(index1);
		int y1 = this.getIndexPositionY(index1);
		int x2 = this.getIndexPositionX(index2);
		int y2 = this.getIndexPositionY(index2);
		
		return this.isPositionInJump(x1, y1, x2, y2);
	}
	
	public boolean isPositionAtDistance(int distance, int x1, int y1, int x2, int y2) {
		Double calculatedDistance = Math.sqrt(
				Math.pow(Math.abs(x1 - x2), 2) + 
				Math.pow(Math.abs(y1 - y2), 2));
		
		return calculatedDistance.intValue() == distance;
	}
	
	public boolean isPositionAtDistance(int distance, int index1, int index2) {
		int x1 = this.getIndexPositionX(index1);
		int y1 = this.getIndexPositionY(index1);
		int x2 = this.getIndexPositionX(index2);
		int y2 = this.getIndexPositionY(index2);
		
		return isPositionAtDistance(distance, x1, y1, x2, y2);
	}
	
	/**
	 * Check if the (x2,y2) position can be obtained from (x1, y1) position 
	 * by rotations and reflections of the board.
	 * 
	 * We will check 90º, 180º and 270º rotations.
	 * 
	 * @param x1 Row for first position
	 * @param y1 Column for first position
	 * @param x2 Row for the second position
	 * @param y2 Column for the second position
	 * @return true if (x2,y2) is a rotation of the (x1,y1) position
	 */
	public boolean isPositionSymmetric(int x1, int y1, int x2, int y2) {
		int index1 = this.getPositionIndex(x1, y1);
		int index2 = this.getPositionIndex(x2, y2);
		
		return this.isPositionSymmetric(index1, index2);
	}
	
	/**
	 * Checks if two positions are symmetric and we can get to index2
	 * thought rotations or reflections of the board.
	 * 
	 * @param index1
	 * @param index2
	 * @return
	 */
	public boolean isPositionSymmetric(int index1, int index2) {
		int[] symmetryIndexes = this.indexSymmetryCache.get(index1);
		// The indexes are sorted, so we can use binary search
		int pos = Arrays.binarySearch(symmetryIndexes, index2);
		return pos >= 0;
	}
	
	public boolean canPieceTakeOut(PieceTypes piece, int pieceIndex, int targetIndex) {
		
		boolean canTakeOut = false;
		
		if (pieceIndex == targetIndex) {
			// Avoid checking if both positions are the same, pieces does not 
			// threaten themselves.
			return canTakeOut;
		}
		
		int x1 = this.getIndexPositionX(pieceIndex);
		int y1 = this.getIndexPositionY(pieceIndex);
		int x2 = this.getIndexPositionX(targetIndex);
		int y2 = this.getIndexPositionY(targetIndex);
		
		switch(piece) {
			case BISHOP:
				canTakeOut = isPositionInDiagonal(x1, y1, x2, y2);
				break;
			case KING:
				canTakeOut = isPositionAtDistance(1, x1, y1, x2, y2);
				break;
			case KNIGHT:
				canTakeOut = isPositionInJump(x1, y1, x2, y2);
				break;
			case QUEEN:
				canTakeOut = isPositionInDiagonal(x1, y1, x2, y2)
								|| isPositionInLine(x1, y1, x2, y2);
				break;
			case ROOK:
				canTakeOut = isPositionInLine(x1, y1, x2, y2);
				break;
			default:
				throw new UnsupportedChessPieceTypeException("Unsupported chess piece: " + piece);
		}
		
		return canTakeOut;
	}
	
	/**
	 * Checks if a bishop can take a piece in the given position
	 * 
	 * Bishops take out pieces in diagonal, that is, a piece is taken by a 
	 * bishop if the absolute difference of the x coordinates of each
	 * position are equal to the absolute difference of the y coordinates
	 * of each position
	 * 
	 * @param position Position of the piece to check if it can be taken
	 * @return
	 */
	public boolean canBishopTakeOut(int pieceIndex, int targetIndex) {
		
		return canPieceTakeOut(PieceTypes.BISHOP, pieceIndex, targetIndex);
	}
	
	public boolean canKingTakeOut(int pieceIndex, int targetIndex) {
		
		return canPieceTakeOut(PieceTypes.KING, pieceIndex, targetIndex);
	}
	
	public boolean canKnightTakeOut(int pieceIndex, int targetIndex) {
		return canPieceTakeOut(PieceTypes.KNIGHT, pieceIndex, targetIndex);
	}
	
	public boolean canQueenTakeOut(int pieceIndex, int targetIndex) {
		return canPieceTakeOut(PieceTypes.QUEEN, pieceIndex, targetIndex);
	}
	
	public boolean canRookTakeOut(int pieceIndex, int targetIndex) {
		return canPieceTakeOut(PieceTypes.ROOK, pieceIndex, targetIndex);
	}

	/**
	 * Generates all symmetric solutions for a given solution.
	 * @param solution
	 * @return
	 */
	public int generateSymmetricsSolutions(BtSolution solution, Map<String, BtSolution> currentSolutions) {
		
		ArrayList<BtSolution> symmetricSolutions = new ArrayList<BtSolution>(10);
		
		// Reflect
		BtSolution reflect = generateSymmetricSolution(solution, SymmetryOperations.REFLECT);
		if (!reflect.equals(solution) && !currentSolutions.containsKey(reflect.getHash())) {
			symmetricSolutions.add(reflect);
		}
		
		// Rotate 90º
		BtSolution rotated90 = generateSymmetricSolution(solution, SymmetryOperations.ROTATE);
		if(!rotated90.equals(solution) 
				&& !symmetricSolutions.contains(rotated90)
				&& !currentSolutions.containsKey(rotated90.getHash())) {
			symmetricSolutions.add(rotated90);
		}
		
		// Reflect rotate 90º
		BtSolution reflect90 = generateSymmetricSolution(rotated90, SymmetryOperations.REFLECT);
		if(!reflect90.equals(solution) 
				&& !symmetricSolutions.contains(reflect90)
				&& !currentSolutions.containsKey(reflect90.getHash())) {
			symmetricSolutions.add(reflect90);
		}
		
		// Rotate 180
		BtSolution rotated180 = generateSymmetricSolution(rotated90, SymmetryOperations.ROTATE);
		if(!rotated180.equals(solution) 
				&& !symmetricSolutions.contains(rotated180)
				&& !currentSolutions.containsKey(rotated180.getHash())) {
			symmetricSolutions.add(rotated180);
		}
		
		// Reflect rotate 180º
		BtSolution reflect180 = generateSymmetricSolution(rotated180, SymmetryOperations.REFLECT);
		if(!reflect180.equals(solution) 
				&& !symmetricSolutions.contains(reflect180)
				&& !currentSolutions.containsKey(reflect180.getHash())) {
			symmetricSolutions.add(reflect180);
		}
		
		// Rotate 270
		BtSolution rotated270 = generateSymmetricSolution(rotated180, SymmetryOperations.ROTATE);
		if(!rotated270.equals(solution) 
				&& !symmetricSolutions.contains(rotated270)
				&& !currentSolutions.containsKey(rotated270.getHash())) {
			symmetricSolutions.add(rotated270);
		}
		
		// Reflect rotate 270º
		BtSolution reflect270 = generateSymmetricSolution(rotated270, SymmetryOperations.REFLECT);
		if(!reflect270.equals(solution) 
				&& !symmetricSolutions.contains(reflect270)
				&& !currentSolutions.containsKey(reflect270.getHash())) {
			symmetricSolutions.add(reflect270);
		}
		
		for(BtSolution symmetricSolution: symmetricSolutions) {
			currentSolutions.put(symmetricSolution.getHash(), symmetricSolution);
		}
		
		return symmetricSolutions.size();
	}

	private BtSolution generateSymmetricSolution(BtSolution solution,
			SymmetryOperations operation) {
		
		PieceTypes[] board = new PieceTypes[this.boardHeight * this.boardWidth];
		
		for (int x= 0; x<this.boardHeight; x++) {
			for (int y=0; y<this.boardWidth; y++) {
				
				int index = this.getPositionIndex(x, y);
				int indexOp = 0;
				
				if (operation == SymmetryOperations.ROTATE) {
					indexOp = this.getPositionIndex(y, this.boardWidth - 1 - x); 
				}
				else if (operation == SymmetryOperations.REFLECT) {
					indexOp = this.getPositionIndex(x, this.boardWidth - 1 - y);
				}
				else {
					throw new UnsupportedOperationException("Cannot apply operation " + operation);
				}
				
				board[indexOp] = solution.getBoard()[index];
			}
		}
		
		BtSolution newSolution = new BtSolution(board);
		return newSolution;
	}
}
