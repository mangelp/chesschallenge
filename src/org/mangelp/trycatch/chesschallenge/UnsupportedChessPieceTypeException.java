package org.mangelp.trycatch.chesschallenge;

public class UnsupportedChessPieceTypeException extends RuntimeException {
	
	private static final long serialVersionUID = -3060462845142276026L;

	public UnsupportedChessPieceTypeException(String string) {
		super(string);
	}
}
