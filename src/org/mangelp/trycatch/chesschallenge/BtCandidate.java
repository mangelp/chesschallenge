package org.mangelp.trycatch.chesschallenge;

public class BtCandidate {
	private PieceTypes pieceType;
	private int index;
	
	public PieceTypes getPieceType() {
		return pieceType;
	}
	
	public void setPieceType(PieceTypes pieceType) {
		this.pieceType = pieceType;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}

	public BtCandidate(PieceTypes pieceType, int index) {
		this.pieceType = pieceType;
		this.index = index;
	}
	
	public String toString() {
		return pieceType + "@" + index;
	}
}
