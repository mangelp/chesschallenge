package org.mangelp.trycatch.chesschallenge;

public enum PieceTypes {
	BISHOP(0, 'B'),
	KING(1, 'K'),
	KNIGHT(2, 'N'),
	QUEEN(3, 'Q'),
	ROOK(4, 'R');
	
	private final int value;
	
	private final char abreviation;
	
	public int getValue() {
		return value;
	}
	
	public char getAbreviation() {
		return abreviation;
	}
	
	private PieceTypes(int value, char abreviation) {
		this.value = value;
		this.abreviation = abreviation;
	}
	
	public String toString() {
		// Capitalize name
		String str = super.toString();
		
		str = str.substring(0, 1).toUpperCase()
				+ str.substring(1).toLowerCase();
		
		return str;
	}

	public static PieceTypes fromString(String str) {
		if (str == null || str.isEmpty()) {
			return null;
		}
		
		str = str.toLowerCase();
		
		if (str.equals("b") || str.equals("bishop")) {
			return BISHOP;
		}
		
		if (str.equals("k") || str.equals("king")) {
			return KING;
		}
		
		if (str.equals("n") || str.equals("knight")) {
			return KNIGHT;
		}
		
		if (str.equals("q") || str.equals("queen")) {
			return QUEEN;
		}
		
		if (str.equals("r") || str.equals("rook")) {
			return ROOK;
		}
				
		return null;
	}
}
