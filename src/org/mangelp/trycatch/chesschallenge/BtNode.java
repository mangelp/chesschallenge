package org.mangelp.trycatch.chesschallenge;

import java.util.HashMap;

/**
 * Models a single node in the backtracking tree.
 * 
 * @author Miguel Angel Perez
 *
 */
public class BtNode implements Cloneable {
	/**
	 * Map of available number of pieces by each piece type
	 */
	private HashMap<PieceTypes, Integer> availablePieces;
	
	/**
	 * Board representation as a plain array. Position X(row), Y(column) is calculated as 
	 * X*N + Y where N is the width of the board as squares.
	 */
	private PieceTypes[] board;
	
	/**
	 * Last piece type added.
	 * Null means nothing was previously added
	 */
	private PieceTypes lastPieceType = null;
	
	/**
	 * Position were the last piece type was added. 
	 * -1 means nothing was previously added.
	 */
	private int lastIndex = -1;
	
	private int addedPiecesCount;
	
	private long numSolutions = 0;
	
	private int depth = 0;
	private int childPosition = 0;
	private int nextChildPosition = 0;
	
	public HashMap<PieceTypes, Integer> getAvailablePieces() {
		return availablePieces;
	}
	
	public PieceTypes[] getBoard() {
		return board;
	}
	
	public int getLastIndex() {
		return lastIndex;
	}
	
	public PieceTypes getLastPieceType() {
		return lastPieceType;
	}
	
	public int getAddedPiecesCount() {
		return addedPiecesCount;
	}
	
	public long getNumSolutions() {
		return numSolutions;
	}
	
	public void setNumSolutions(long numSolutions) {
		this.numSolutions = numSolutions;
	}
	
	protected BtNode() {
		
	}
	
	public void addPieceAtPosition(PieceTypes type, int index) {
		
		if (lastPieceType != null || lastIndex != -1) {
			throw new IllegalStateException("Cannot overwrite the last piece added, remove it first.");
		}
		
		if (this.board[index] != null) {
			throw new IllegalStateException(
					"The board index " + index + " is already set to " + this.board[index]);
		}
		
		int available = this.availablePieces.get(type);
		
		if (available<= 0) {
			throw new IllegalStateException(
					"There are no available pieces of type " + type);
		}
		
		this.board[index] = type;
		this.availablePieces.put(type, available-1);
		
		// Note down the last piece and index set
		this.lastPieceType = type;
		this.lastIndex = index;
		this.addedPiecesCount++;
	}
	
	/**
	 * Removes a piece from the board
	 * 
	 * @param type
	 * @param index
	 * @return true if the piece exists and is removed or false if it didn't 
	 * existed an therefore could not be removed
	 */
	public boolean removePieceFromPosition(PieceTypes type, int index) {
		if (this.board[index] == type) {
			int available = this.availablePieces.get(type);
			this.availablePieces.put(type, available+1);
			
			this.board[index] = null;
			this.addedPiecesCount--;
			
			return true;
		}
		
		return false;
	}

	public void removeLastAddedPiece() {
		if (this.lastIndex == -1 || this.lastPieceType == null) {
			throw new IllegalStateException("No piece have been added");
		}
		
		this.removePieceFromPosition(this.lastPieceType, this.lastIndex);
		this.lastIndex = -1;
		this.lastPieceType = null;
	}
	
	public BtNode(ChessProblem problem) {
		this.board = new PieceTypes[problem.getBoardWidth() * problem.getBoardHeight()];
		
		for(int i=0; i<this.board.length; i++) {
			this.board[i] = null;
		}
		
		this.availablePieces = new HashMap<PieceTypes, Integer>(problem.getAvailablePieces());
	}
	
	/**
	 * Gets a full copy of the current node cloning it and clearing the 
	 * values of internal fields of last added index and piece so another
	 * piece can be added in the next step.
	 * 
	 * @return Current node full copy
	 */
	public BtNode getNextNodeCopy() {
		BtNode node = new BtNode();
		node.availablePieces = new HashMap<PieceTypes, Integer>(this.availablePieces);
		node.lastPieceType = this.lastPieceType;
		node.lastIndex = this.lastIndex;
		node.addedPiecesCount = this.addedPiecesCount; 
		node.board = new PieceTypes[this.board.length];
		node.lastIndex = -1;
		node.lastPieceType = null;
		
		for(int i=0; i<node.board.length; i++) {
			node.board[i] = this.board[i];
		}
		
		node.depth = this.depth + 1;
		node.childPosition = this.nextChildPosition++;
		
		return node;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Node[" + depth + "," + this.childPosition + "]");
		
		for(int i=0; i<this.board.length; i++) {
			if (board[i] != null) {
				sb.append(board[i] + "@" + i + ",");
			}
		}
		
		return sb.toString();
	}
}
