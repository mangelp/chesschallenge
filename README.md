
This repo solves a given chess problem using a Java console application.

The problem is to find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others. Assume the colour of the piece does not matter, and that there are no pawns among the pieces.

Write a program which takes as input:

* The dimensions of the board: M, N
* The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and place on the board.
* As output, the program should list all the unique configurations to the console for which all of the pieces can be placed on the board without threatening each other.

There is a jar in /lib folder named chesschallenge.jar that contains a build of the code.

The command line interface is very simple. It requires the number of squares per side of the board and then a comma-separated list of piece types and their quantity separated by an `@`.

An example of execution into a fx-8320 with 16 GB RAM for a 7x7 board with 2 kings, 2 queens, 2 bishops and 1 knight is: 

```
$ java -jar chesschallenge.jar 7 K@2,Q@2,B@2,N@1
 * Unique solutions: 3063828
 * Time spent: 17242 ms
```

The above command doesn't prints any solutions to stdout. Printing them has an overhead that is added up to the measured execution time. To enable printing to stdout you simply have to add the --print-solutions argument to the end.

```
$ java -jar ./chesschallenge.jar 3 k@2,R@1 --print-solutions
+---+---+---+
|   | R |   |
|   |   |   |
| K |   | K |
+---+---+---+

+---+---+---+
|   |   | K |
| R |   |   |
|   |   | K |
+---+---+---+

+---+---+---+
| K |   |   |
|   |   | R |
| K |   |   |
+---+---+---+

+---+---+---+
| K |   | K |
|   |   |   |
|   | R |   |
+---+---+---+

 * Unique solutions: 4
 * Time spent: 96 ms

```

Without printing the solutions the time is quite smaller:

```
[mangelp@ouroboros lib]$ java -jar ./chesschallenge.jar 3 k@2,R@1
 * Unique solutions: 4
 * Time spent: 13 ms
```

The difference in time comes from the code that prints the ascii table. This code uses the [java-ascii-table](https://code.google.com/p/java-ascii-table/) library found on google code and included as a jar file in the /lib folder.

